import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  try {
    const infoElement = createElement({ tagName: 'div', className: 'fighter-preview___info' });
    const imageElement = createFighterImage(fighter);
    infoElement.innerHTML = `
    <span>Name: <strong>${fighter.name}</strong></span>
    <span>Health: <strong>${fighter.health}</strong></span>
    <span>Attack: <strong>${fighter.attack}</strong></span>
    <span>Defense: <strong>${fighter.defense}</strong></span>
    `
    fighterElement.append(imageElement, infoElement);
  } catch {

  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
