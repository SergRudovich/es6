import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  const firstFighterIndicator = document.getElementById('left-fighter-indicator');
  const secondFighterIndicator = document.getElementById('right-fighter-indicator');
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let pressed = new Set();
    let isKeyPressed = true;
    let isCanCriticalFirst = true;
    let isCanCriticalSecond = true;
    let isFirstBlock = false;
    let isSecondBlock = false;

    document.addEventListener('keydown', function (event) {
      pressed.add(event.code);

      if (isKeyPressed) {
        switch (event.code) {
          case controls.PlayerOneAttack:
            if (!isFirstBlock && !isSecondBlock) {
              secondFighterHealth -= getDamage(firstFighter, secondFighter);
              setBar('second');
              console.log(`${firstFighter.name} - bum!`);
            } else {
              if (isFirstBlock) {
                console.log(`${firstFighter.name} is blocked and can not bum`);
              } else {
                console.log(`${secondFighter.name} is blocked`)
              }
            }
            isKeyPressed = false;
            break;
          case controls.PlayerOneBlock:
            isFirstBlock = true;
            break;
          case controls.PlayerTwoAttack:
            if (!isFirstBlock && !isSecondBlock) {
              firstFighterHealth -= getDamage(secondFighter, firstFighter);
              setBar('first');
              console.log(`${secondFighter.name} - bum!`);
            } else {
              if (isSecondBlock) {
                console.log(`${secondFighter.name} is blocked and can not bum`)
              } else {
                console.log(`${firstFighter.name} is blocked`);
              }
            }
            isKeyPressed = false;
            break;
          case controls.PlayerTwoBlock:
            isSecondBlock = true;
            break;
        }
      }

      if (isCombinationPressed(controls.PlayerOneCriticalHitCombination) && isCanCriticalFirst) {
        console.log(`${firstFighter.name} - !!!!!!!!! Critical Bum---!`);
        secondFighterHealth -= firstFighter.attack * 2;
        isCanCriticalFirst = false;
        setBar('second');
        setTimeout(() => isCanCriticalFirst = true, 10000);
      }
      if (isCombinationPressed(controls.PlayerTwoCriticalHitCombination) && isCanCriticalSecond) {
        console.log(`${secondFighter.name} - !!!!!!!!! Critical Bum---!`);
        firstFighterHealth -= secondFighter.attack * 2;
        isCanCriticalSecond = false;
        setBar('first');
        setTimeout(() => isCanCriticalSecond = true, 10000);
      }

    });

    document.addEventListener('keyup', function (event) {
      isKeyPressed = true;
      if (event.code === controls.PlayerOneBlock) isFirstBlock = false;
      if (event.code === controls.PlayerTwoBlock) isSecondBlock = false;
      pressed.delete(event.code);
    });

    function isCombinationPressed(combination) {
      for (let code of combination) {
        if (!pressed.has(code)) {
          return false;
        }
      }
      pressed.clear();
      return true;
    }

    function setBar(player) {
      switch (player) {
        case 'first':
          if (firstFighterHealth <= 0) {
            firstFighterIndicator.style.width = '0';
            resolve(secondFighter);
          }
          let firstHelthPercent = (firstFighterHealth * 100) / firstFighter.health;
          firstFighterIndicator.style.width = `${firstHelthPercent}%`;
          if (firstHelthPercent < 30) firstFighterIndicator.style.backgroundColor = "red";
          break;
        case 'second':
          if (secondFighterHealth <= 0) {
            secondFighterIndicator.style.width = '0';
            resolve(firstFighter);
          }
          let secondHelthPercent = (secondFighterHealth * 100) / secondFighter.health;
          secondFighterIndicator.style.width = `${secondHelthPercent}%`;
          if (secondHelthPercent < 30) secondFighterIndicator.style.backgroundColor = "red";
          break;
      }
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage < 0) ? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

