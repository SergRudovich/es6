import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createElement({ tagName: 'div' });
  bodyElement.style.background='#ffdfa7';
  const nameElement = createElement({ tagName: 'h1' });
  nameElement.innerText = `${fighter.name}`;
  nameElement.style.textAlign = 'center';
  const imgElement = createFighterImage(fighter);
  bodyElement.append(nameElement, imgElement);
  showModal({title: 'Winner', bodyElement, onClose: () => {location.reload()}});
}
